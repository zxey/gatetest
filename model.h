#ifndef MODEL_H
#define MODEL_H

#include "cable.h"

#include <QQmlListProperty>
#include <QObject>

class Model : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QQmlListProperty<Cable> cables READ getCables CONSTANT)
public:
    explicit Model(QObject *parent = nullptr);
    static Model& getInstance()
    {
        static Model instance;
        return instance;
    }

public slots:
    QQmlListProperty<Cable> getCables();
private:
    QList<Cable*> cables;
};

#endif // MODEL_H
