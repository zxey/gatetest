#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "cable.h"
#include "cableside.h"
#include "model.h"

#define REGISTER_TYPE(type) qmlRegisterUncreatableType<type>("Gatetest", 1, 0, #type, #type " can't be created.")
#define REGISTER_SINGLETON(Type) qmlRegisterSingletonType<Type>("Gatetest", 1, 0, #Type, [](QQmlEngine *, QJSEngine *) -> QObject* \
                                                                                        { \
                                                                                            Type* instance = &Type::getInstance(); \
                                                                                            QQmlEngine::setObjectOwnership(instance, QQmlEngine::CppOwnership); \
                                                                                            return instance; \
                                                                                        })

int main(int argc, char *argv[])
{
    qputenv("QT_IM_MODULE", QByteArray("qtvirtualkeyboard"));

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    REGISTER_TYPE(Cable);
    REGISTER_TYPE(CableSide);
    REGISTER_SINGLETON(Model);

    QQmlApplicationEngine engine;
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
