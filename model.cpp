#include "model.h"

Model::Model(QObject *parent) : QObject(parent)
{
    for (int i = 0; i < 9*9; i++)
    {
        this->cables.append(new Cable(this));
    }
}

QQmlListProperty<Cable> Model::getCables()
{
    return QQmlListProperty<Cable>(this, this->cables);
}
