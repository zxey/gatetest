/****************************************************************************
** Meta object code from reading C++ file 'cable.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../cable.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cable.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_Cable_t {
    QByteArrayData data[11];
    char stringdata0[74];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_Cable_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_Cable_t qt_meta_stringdata_Cable = {
    {
QT_MOC_LITERAL(0, 0, 5), // "Cable"
QT_MOC_LITERAL(1, 6, 7), // "getLeft"
QT_MOC_LITERAL(2, 14, 10), // "CableSide*"
QT_MOC_LITERAL(3, 25, 0), // ""
QT_MOC_LITERAL(4, 26, 8), // "getRight"
QT_MOC_LITERAL(5, 35, 6), // "getTop"
QT_MOC_LITERAL(6, 42, 9), // "getBottom"
QT_MOC_LITERAL(7, 52, 4), // "left"
QT_MOC_LITERAL(8, 57, 5), // "right"
QT_MOC_LITERAL(9, 63, 3), // "top"
QT_MOC_LITERAL(10, 67, 6) // "bottom"

    },
    "Cable\0getLeft\0CableSide*\0\0getRight\0"
    "getTop\0getBottom\0left\0right\0top\0bottom"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Cable[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       4,   38, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   34,    3, 0x0a /* Public */,
       4,    0,   35,    3, 0x0a /* Public */,
       5,    0,   36,    3, 0x0a /* Public */,
       6,    0,   37,    3, 0x0a /* Public */,

 // slots: parameters
    0x80000000 | 2,
    0x80000000 | 2,
    0x80000000 | 2,
    0x80000000 | 2,

 // properties: name, type, flags
       7, 0x80000000 | 2, 0x00095409,
       8, 0x80000000 | 2, 0x00095409,
       9, 0x80000000 | 2, 0x00095409,
      10, 0x80000000 | 2, 0x00095409,

       0        // eod
};

void Cable::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Cable *_t = static_cast<Cable *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: { CableSide* _r = _t->getLeft();
            if (_a[0]) *reinterpret_cast< CableSide**>(_a[0]) = std::move(_r); }  break;
        case 1: { CableSide* _r = _t->getRight();
            if (_a[0]) *reinterpret_cast< CableSide**>(_a[0]) = std::move(_r); }  break;
        case 2: { CableSide* _r = _t->getTop();
            if (_a[0]) *reinterpret_cast< CableSide**>(_a[0]) = std::move(_r); }  break;
        case 3: { CableSide* _r = _t->getBottom();
            if (_a[0]) *reinterpret_cast< CableSide**>(_a[0]) = std::move(_r); }  break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 3:
        case 2:
        case 1:
        case 0:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< CableSide* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        Cable *_t = static_cast<Cable *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< CableSide**>(_v) = _t->getLeft(); break;
        case 1: *reinterpret_cast< CableSide**>(_v) = _t->getRight(); break;
        case 2: *reinterpret_cast< CableSide**>(_v) = _t->getTop(); break;
        case 3: *reinterpret_cast< CableSide**>(_v) = _t->getBottom(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject Cable::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Cable.data,
      qt_meta_data_Cable,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *Cable::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Cable::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_Cable.stringdata0))
        return static_cast<void*>(const_cast< Cable*>(this));
    return QObject::qt_metacast(_clname);
}

int Cable::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 4)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 4;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 4;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 4;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
