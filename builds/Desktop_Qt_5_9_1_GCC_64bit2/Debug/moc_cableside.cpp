/****************************************************************************
** Meta object code from reading C++ file 'cableside.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.9.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../../../cableside.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'cableside.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.9.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
QT_WARNING_PUSH
QT_WARNING_DISABLE_DEPRECATED
struct qt_meta_stringdata_CableSide_t {
    QByteArrayData data[15];
    char stringdata0[165];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    qptrdiff(offsetof(qt_meta_stringdata_CableSide_t, stringdata0) + ofs \
        - idx * sizeof(QByteArrayData)) \
    )
static const qt_meta_stringdata_CableSide_t qt_meta_stringdata_CableSide = {
    {
QT_MOC_LITERAL(0, 0, 9), // "CableSide"
QT_MOC_LITERAL(1, 10, 13), // "outputChanged"
QT_MOC_LITERAL(2, 24, 0), // ""
QT_MOC_LITERAL(3, 25, 15), // "positionChanged"
QT_MOC_LITERAL(4, 41, 16), // "connectedChanged"
QT_MOC_LITERAL(5, 58, 9), // "getOutput"
QT_MOC_LITERAL(6, 68, 9), // "setOutput"
QT_MOC_LITERAL(7, 78, 6), // "output"
QT_MOC_LITERAL(8, 85, 11), // "getPosition"
QT_MOC_LITERAL(9, 97, 11), // "setPosition"
QT_MOC_LITERAL(10, 109, 8), // "position"
QT_MOC_LITERAL(11, 118, 12), // "getConnected"
QT_MOC_LITERAL(12, 131, 10), // "CableSide*"
QT_MOC_LITERAL(13, 142, 12), // "setConnected"
QT_MOC_LITERAL(14, 155, 9) // "connected"

    },
    "CableSide\0outputChanged\0\0positionChanged\0"
    "connectedChanged\0getOutput\0setOutput\0"
    "output\0getPosition\0setPosition\0position\0"
    "getConnected\0CableSide*\0setConnected\0"
    "connected"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_CableSide[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       9,   14, // methods
       3,   74, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,   59,    2, 0x06 /* Public */,
       3,    0,   60,    2, 0x06 /* Public */,
       4,    0,   61,    2, 0x06 /* Public */,

 // slots: name, argc, parameters, tag, flags
       5,    0,   62,    2, 0x0a /* Public */,
       6,    1,   63,    2, 0x0a /* Public */,
       8,    0,   66,    2, 0x0a /* Public */,
       9,    1,   67,    2, 0x0a /* Public */,
      11,    0,   70,    2, 0x0a /* Public */,
      13,    1,   71,    2, 0x0a /* Public */,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Bool,
    QMetaType::Void, QMetaType::Bool,    7,
    QMetaType::QPointF,
    QMetaType::Void, QMetaType::QPointF,   10,
    0x80000000 | 12,
    QMetaType::Void, 0x80000000 | 12,   14,

 // properties: name, type, flags
       7, QMetaType::Bool, 0x00495103,
      10, QMetaType::QPointF, 0x00495103,
      14, 0x80000000 | 12, 0x0049510b,

 // properties: notify_signal_id
       0,
       1,
       2,

       0        // eod
};

void CableSide::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        CableSide *_t = static_cast<CableSide *>(_o);
        Q_UNUSED(_t)
        switch (_id) {
        case 0: _t->outputChanged(); break;
        case 1: _t->positionChanged(); break;
        case 2: _t->connectedChanged(); break;
        case 3: { bool _r = _t->getOutput();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = std::move(_r); }  break;
        case 4: _t->setOutput((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 5: { QPointF _r = _t->getPosition();
            if (_a[0]) *reinterpret_cast< QPointF*>(_a[0]) = std::move(_r); }  break;
        case 6: _t->setPosition((*reinterpret_cast< QPointF(*)>(_a[1]))); break;
        case 7: { CableSide* _r = _t->getConnected();
            if (_a[0]) *reinterpret_cast< CableSide**>(_a[0]) = std::move(_r); }  break;
        case 8: _t->setConnected((*reinterpret_cast< CableSide*(*)>(_a[1]))); break;
        default: ;
        }
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 8:
            switch (*reinterpret_cast<int*>(_a[1])) {
            default: *reinterpret_cast<int*>(_a[0]) = -1; break;
            case 0:
                *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< CableSide* >(); break;
            }
            break;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (CableSide::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CableSide::outputChanged)) {
                *result = 0;
                return;
            }
        }
        {
            typedef void (CableSide::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CableSide::positionChanged)) {
                *result = 1;
                return;
            }
        }
        {
            typedef void (CableSide::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&CableSide::connectedChanged)) {
                *result = 2;
                return;
            }
        }
    } else if (_c == QMetaObject::RegisterPropertyMetaType) {
        switch (_id) {
        default: *reinterpret_cast<int*>(_a[0]) = -1; break;
        case 2:
            *reinterpret_cast<int*>(_a[0]) = qRegisterMetaType< CableSide* >(); break;
        }
    }

#ifndef QT_NO_PROPERTIES
    else if (_c == QMetaObject::ReadProperty) {
        CableSide *_t = static_cast<CableSide *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: *reinterpret_cast< bool*>(_v) = _t->getOutput(); break;
        case 1: *reinterpret_cast< QPointF*>(_v) = _t->getPosition(); break;
        case 2: *reinterpret_cast< CableSide**>(_v) = _t->getConnected(); break;
        default: break;
        }
    } else if (_c == QMetaObject::WriteProperty) {
        CableSide *_t = static_cast<CableSide *>(_o);
        Q_UNUSED(_t)
        void *_v = _a[0];
        switch (_id) {
        case 0: _t->setOutput(*reinterpret_cast< bool*>(_v)); break;
        case 1: _t->setPosition(*reinterpret_cast< QPointF*>(_v)); break;
        case 2: _t->setConnected(*reinterpret_cast< CableSide**>(_v)); break;
        default: break;
        }
    } else if (_c == QMetaObject::ResetProperty) {
    }
#endif // QT_NO_PROPERTIES
}

const QMetaObject CableSide::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_CableSide.data,
      qt_meta_data_CableSide,  qt_static_metacall, nullptr, nullptr}
};


const QMetaObject *CableSide::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *CableSide::qt_metacast(const char *_clname)
{
    if (!_clname) return nullptr;
    if (!strcmp(_clname, qt_meta_stringdata_CableSide.stringdata0))
        return static_cast<void*>(const_cast< CableSide*>(this));
    return QObject::qt_metacast(_clname);
}

int CableSide::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 9)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 9;
    }
#ifndef QT_NO_PROPERTIES
   else if (_c == QMetaObject::ReadProperty || _c == QMetaObject::WriteProperty
            || _c == QMetaObject::ResetProperty || _c == QMetaObject::RegisterPropertyMetaType) {
        qt_static_metacall(this, _c, _id, _a);
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyDesignable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyScriptable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyStored) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyEditable) {
        _id -= 3;
    } else if (_c == QMetaObject::QueryPropertyUser) {
        _id -= 3;
    }
#endif // QT_NO_PROPERTIES
    return _id;
}

// SIGNAL 0
void CableSide::outputChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 0, nullptr);
}

// SIGNAL 1
void CableSide::positionChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 1, nullptr);
}

// SIGNAL 2
void CableSide::connectedChanged()
{
    QMetaObject::activate(this, &staticMetaObject, 2, nullptr);
}
QT_WARNING_POP
QT_END_MOC_NAMESPACE
