#ifndef CABLESIDE_H
#define CABLESIDE_H

#include <QObject>
#include <QPointF>

class CableSide : public QObject
{
    Q_OBJECT
    //Q_PROPERTY(bool input  READ getInput  WRITE setInput  NOTIFY inputChanged)
    Q_PROPERTY(bool output READ getOutput WRITE setOutput NOTIFY outputChanged)
    Q_PROPERTY(QPointF position READ getPosition WRITE setPosition NOTIFY positionChanged)
    Q_PROPERTY(CableSide* connected READ getConnected WRITE setConnected NOTIFY connectedChanged)
public:
    explicit CableSide(QObject *parent = nullptr);
public slots:
//    bool getInput();
    bool getOutput();
//    void setInput(bool input);
    void setOutput(bool output);
    QPointF getPosition();
    void setPosition(QPointF position);
    CableSide* getConnected();
    void setConnected(CableSide* connected);
signals:
//    void inputChanged();
    void outputChanged();
    void positionChanged();
    void connectedChanged();
private:
//    bool input;
    bool output;
    QPointF position;
    CableSide* connected;
};

#endif // CABLESIDE_H
