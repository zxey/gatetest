#include "cableside.h"

CableSide::CableSide(QObject *parent)
    : QObject(parent)
    //, input(false)
    , output(false)
    , position(0, 0)
    , connected(nullptr)
{

}

//bool CableSide::getInput()
//{
//    return this->input;
//}

bool CableSide::getOutput()
{
    return this->output;
}

//void CableSide::setInput(bool input)
//{
//    if (this->input != input)
//    {
//        this->input = input;
//        emit this->inputChanged();
//    }
//}

void CableSide::setOutput(bool output)
{
    if (this->output != output)
    {
        this->output = output;
        emit this->outputChanged();
    }
}

QPointF CableSide::getPosition()
{
    return this->position;
}

void CableSide::setPosition(QPointF position)
{
    if (this->position != position)
    {
        this->position = position;
        emit this->positionChanged();
    }
}

CableSide* CableSide::getConnected()
{
    return this->connected;
}

void CableSide::setConnected(CableSide* connected)
{
    this->connected = connected;
    emit this->connectedChanged();
}
