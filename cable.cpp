#include "cable.h"

#include <QtDebug>

Cable::Cable(QObject *parent)
    : QObject(parent)
    , value(false)
{
    sides << left;
    sides << right;
    sides << top;
    sides << bottom;
    startTimer(0);
}

void Cable::timerEvent(QTimerEvent *)
{
    this->resetAllOutputs();
    CableSide* side = this->getFirstOnCable();
    if (side)
    {
        this->setOutputsExcludingSide(side, true);
    }
}

void Cable::setOutputsExcludingSide(CableSide* exclude, bool value)
{
    for (CableSide* side : this->sides)
    {
        if (side != exclude)
        {
            side->setOutput(value);
        }
    }
}

void Cable::setOutputIfNotConnected(CableSide* side, bool value)
{
    if (!side->getConnected())
    {
        side->setOutput(value);
    }
}

void Cable::resetAllOutputs()
{
    for (CableSide* side : this->sides)
    {
        this->setOutputIfNotConnected(side, false);
    }
}

CableSide* Cable::getFirstOnCable()
{
    for (CableSide* side : this->sides)
    {
        if (side->getConnected() && side->getOutput())
        {
            return side;
        }
    }

    return nullptr;
}
