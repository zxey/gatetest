#ifndef CABLE_H
#define CABLE_H

#include "cableside.h"

#include <QObject>

class Cable : public QObject
{
    Q_OBJECT
    Q_PROPERTY(CableSide* left   READ getLeft   CONSTANT)
    Q_PROPERTY(CableSide* right  READ getRight  CONSTANT)
    Q_PROPERTY(CableSide* top    READ getTop    CONSTANT)
    Q_PROPERTY(CableSide* bottom READ getBottom CONSTANT)
    Q_PROPERTY(bool value READ getValue NOTIFY valueChanged)
public:
    explicit Cable(QObject *parent = nullptr);
public slots:
    CableSide* getLeft()   { return &this->left;   }
    CableSide* getRight()  { return &this->right;  }
    CableSide* getTop()    { return &this->top;    }
    CableSide* getBottom() { return &this->bottom; }
    bool getValue() { return this->value; }
protected:
    void timerEvent(QTimerEvent *) override;
private slots:
    void setValue(bool value);
private:
    CableSide left;
    CableSide right;
    CableSide top;
    CableSide bottom;
    bool value;

    QList<CableSide*> sides;

    void setOutputsExcludingSide(CableSide* exclude, bool value);
    void setOutputIfNotConnected(CableSide* side, bool value);
    void resetAllOutputs();
    CableSide* getFirstOnCable();
};

#endif // CABLE_H
