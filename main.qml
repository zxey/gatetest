import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.VirtualKeyboard 2.1
import Gatetest 1.0

ApplicationWindow {
    visible: true
    width: 650
    height: 650
    title: qsTr("Hello World")

    Item {
        anchors.fill: parent
        id: root

        property var firstSide: null
        property var secondSide: null

        function resetSides() {
            firstSide = null
            secondSide = null
        }

        function connectSides() {
            if (firstSide && secondSide) {
                firstSide.connected = secondSide
                secondSide.connected = firstSide

                console.warn("position1", firstSide.position)
                console.warn("position2", secondSide.position)

                firstSide = null
                secondSide = null
                canvas.requestPaint()
            }
        }

        function assignSide(side) {
            if (!firstSide) {
                firstSide = side
            } else if (!secondSide) {
                secondSide = side
                connectSides()
            }
        }

        Grid {
            id: grid
            anchors.centerIn: parent
            columns: 9
            rows: 9
            spacing: 10
            Repeater {
                model: 9*9
                Rectangle {
                    height: 50
                    width: 50
                    border.width: 1
                    border.color: "black"

                    property var cable: Model.cables[index]

                    Rectangle {
                        anchors.centerIn: parent
                        height: parent.height / 2
                        width: parent.width / 2
                        color: cable.value ? "red" : Qt.lighter("red")
                    }

                    Rectangle {
                        id: topRekt
                        anchors.top: parent.top
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height / 4
                        width: parent.width / 2
                        color: "pink"

                        property var side: cable.top
                        property real realX: grid.x + parent.x + x + width / 2
                        property real realY: grid.y + parent.y + y + height / 2
                        property point globalPos: Qt.point(realX, realY)
                        onGlobalPosChanged: {
                            side.position = globalPos
                            console.warn(globalPos)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                console.log("connecting top")
                                root.assignSide(parent.side)
                            }
                        }
                    }

                    Rectangle {
                        id: bottomRekt
                        anchors.bottom: parent.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                        height: parent.height / 4
                        width: parent.width / 2
                        color: "yellow"

                        property var side: cable.bottom
                        property real realX: grid.x + parent.x + x + width / 2
                        property real realY: grid.y + parent.y + y + height / 2
                        property point globalPos: Qt.point(realX, realY)
                        onGlobalPosChanged: {
                            side.position = globalPos
                            console.warn(globalPos)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                console.log("connecting bottom")
                                root.assignSide(parent.side)
                            }
                        }
                    }

                    Rectangle {
                        id: leftRekt
                        anchors.left: parent.left
                        anchors.verticalCenter: parent.verticalCenter
                        height: parent.height / 2
                        width: parent.width / 4
                        color: "lightgreen"

                        property var side: cable.left
                        property real realX: grid.x + parent.x + x + width / 2
                        property real realY: grid.y + parent.y + y + height / 2
                        property point globalPos: Qt.point(realX, realY)
                        onGlobalPosChanged: {
                            side.position = globalPos
                            console.warn(globalPos)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                console.log("connecting left")
                                root.assignSide(parent.side)
                            }
                        }
                    }

                    Rectangle {
                        id: rightRekt
                        anchors.right: parent.right
                        anchors.verticalCenter: parent.verticalCenter
                        height: parent.height / 2
                        width: parent.width / 4
                        color: "lightblue"

                        property var side: cable.right
                        property real realX: grid.x + parent.x + x + width / 2
                        property real realY: grid.y + parent.y + y + height / 2
                        property point globalPos: Qt.point(realX, realY)
                        onGlobalPosChanged: {
                            side.position = globalPos
                            console.warn(globalPos)
                        }

                        MouseArea {
                            anchors.fill: parent
                            onClicked: {
                                console.log("connecting right")
                                root.assignSide(parent.side)
                            }
                        }
                    }
                }
            }
        }

        Canvas {
            id: canvas
            anchors.fill: parent

            function drawSide(ctx, side) {
                if (side.connected) {
                    ctx.beginPath()
                    ctx.moveTo(side.position.x, side.position.y)
                    ctx.lineTo(side.connected.position.x, side.connected.position.y);
                    ctx.closePath()
                    ctx.stroke()
                }
            }

            onPaint: {
                var ctx = getContext("2d");
                ctx.stokeStyle = "black"
                ctx.lineWidth = 1
                for (var i = 0; i < Model.cables.length; i++) {
                    var cable = Model.cables[i]
                    drawSide(ctx, cable.left)
                    drawSide(ctx, cable.right)
                    drawSide(ctx, cable.top)
                    drawSide(ctx, cable.bottom)
                }
            }
        }

        Button {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            text: "Reset"
            onClicked: resetSides()
        }
    }
}
